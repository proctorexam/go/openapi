type StartReviewResponse struct {
  r http.ResponseWriter
}
func (w StartReviewResponse) OK(session *Session) {
  w.r.WriteHeader(200)
  if err := json.NewEncoder(w.r).Encode(session); err != nil {
    log.Println("ERROR: response encoding error: ", err)
  }
}
func (w StartReviewResponse) TemporaryRedirect(session *Session) {
  w.r.WriteHeader(307)
  if err := json.NewEncoder(w.r).Encode(session); err != nil {
    log.Println("ERROR: response encoding error: ", err)
  }
}
func (w StartReviewResponse) Conflict(session *Session) {
  w.r.WriteHeader(409)
  if err := json.NewEncoder(w.r).Encode(session); err != nil {
    log.Println("ERROR: response encoding error: ", err)
  }
}
func (w StartReviewResponse) Default(error *Error) {
  w.r.WriteHeader(400)
  if err := json.NewEncoder(w.r).Encode(error); err != nil {
    log.Println("ERROR: response encoding error: ", err)
  }
}
