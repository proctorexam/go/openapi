type ServiceProvider interface {
  AuthenticateReviewer(ctx context.Context, res *AuthenticateReviewerResponse) error
  StartReview(ctx context.Context, req *StartReviewRequest, res *StartReviewResponse) error
  ResumeReview(ctx context.Context, req *ResumeReviewRequest, res *ResumeReviewResponse) error
  FinishReview(ctx context.Context, req *FinishReviewRequest, res *FinishReviewResponse) error
  CancelReview(ctx context.Context, res *CancelReviewResponse) error
}
