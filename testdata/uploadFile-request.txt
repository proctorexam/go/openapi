type UploadFileRequest struct {
  PetId int64
  AdditionalMetadata string
  Payload io.Reader
}
func (out *UploadFileRequest) Decode(in *http.Request) error {
  parts := strings.Split(in.URL.Path, "/")
  if len(parts) > 2 {
    out.petId = parts[2]
  }
  out.AdditionalMetadata = in.URL.Query.Get("additionalMetadata")
  out.Payload = in.Body
  return nil
}
