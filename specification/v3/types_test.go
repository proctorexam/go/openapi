package specification_v3

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fastjson"
	"gitlab.com/proctorexam/go/openapi/internal/ioutil"
	"gitlab.com/proctorexam/go/openapi/internal/jsonschema"
)

func TestFindNativeType(t *testing.T) {
	type args struct {
		schema   *fastjson.Value
		document *fastjson.Value
	}
	doc := fastjson.MustParse(ioutil.MustReadFile("../../testdata/parameters-parsed.json"))
	doc2 := jsonschema.MustParseFile("../../testdata/review.yaml", false)
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"scalar string",
			args{
				schema:   doc.Get("components", "parameters", "UserName", "schema"),
				document: doc,
			},
			"string",
		},
		{
			"array int64",
			args{
				schema:   doc.Get("components", "parameters", "Token", "schema"),
				document: doc,
			},
			"[]int64",
		},
		{
			"struct",
			args{
				schema:   doc.Get("components", "parameters", "CoordinatesMap", "schema"),
				document: doc,
			},
			"struct",
		},
		{
			"pointer",
			args{
				schema:   doc.Get("components", "parameters", "CoordinatesMap", "schema", "properties", "map"),
				document: doc,
			},
			"*Map",
		},
		{
			"array of pointers",
			args{
				schema:   doc2.Get("components", "schemas", "Review", "properties", "incidents"),
				document: doc2,
			},
			"[]*Incident",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := FindNativeType(tt.args.schema, tt.args.document)
			assert.Equal(t, tt.want, got)
		})
	}
}
