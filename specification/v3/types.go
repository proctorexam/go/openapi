package specification_v3

import (
	"fmt"

	"github.com/valyala/fastjson"
	"gitlab.com/proctorexam/go/openapi/internal/jsonschema"
)

func FindNativeType(schema, document *fastjson.Value) string {
	if schema == nil || document == nil {
		return ""
	}

	sType := str(schema, "type")
	sFormat := str(schema, "format")
	switch sType {
	case "boolean":
		return "bool"
	case "number":
		switch sFormat {
		case "float":
			return "float32"
		case "double":
			return "float64"
		default:
			return "float32"
		}
	case "integer":
		switch sFormat {
		case "int32":
			return "int32"
		case "int64":
			return "int64"
		default:
			return "int"
		}
	case "string":
		switch sFormat {
		case "date":
			return "time.Time"
		case "date-time":
			return "time.Time"
		case "password":
			return "string"
		case "binary":
			return "[]byte"
		case "email":
			return "string"
		case "uuid":
			return "string"
		case "uri":
			return "string"
		case "hostname":
			return "string"
		case "ipv4":
			return "string"
		case "ipv6":
			return "string"
		case "byte":
			return "byte"
		}
		return "string"
	case "object":
		if schema.Exists("additionalProperties") {
			return "map[string]any"
		}
		return "struct"
	case "array":
		if len(schema.GetArray("items")) > 0 {
			return "[]any"
		}
		items := schema.Get("items")
		if itemSchemaType := str(items, "type"); itemSchemaType != "" {
			if itemType := FindNativeType(items, document); itemType != "" {
				return fmt.Sprintf("[]%s", itemType)
			}
		}
		if ref := items.Get("$ref"); ref != nil {
			if itemSchema := jsonschema.Resolve(document, ref); itemSchema != nil {
				keys, end := jsonschema.ParseRef(ref)
				if len(keys) > 0 {
					return fmt.Sprintf("[]*%s", eName(keys[end]))
				}
				if itemType := FindNativeType(itemSchema, document); itemType != "" {
					return fmt.Sprintf("[]%s", itemType)
				}
			}
		}
		return ""
	default:
		if ref := schema.Get("$ref"); ref != nil {
			keys, end := jsonschema.ParseRef(ref)
			if len(keys) > 0 {
				return fmt.Sprintf("*%s", eName(keys[end]))
			}
		}
		return ""
	}
}
