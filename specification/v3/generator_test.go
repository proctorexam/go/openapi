package specification_v3

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fastjson"
	"gitlab.com/proctorexam/go/openapi/internal/ioutil"
	"gitlab.com/proctorexam/go/openapi/internal/jsonschema"
)

func TestGenerator_Type(t *testing.T) {
	type args struct {
		name   string
		schema *fastjson.Value
		tag    []tagFunc
	}
	doc := jsonschema.MustParseFile("../../testdata/review.yaml", false)
	tests := []struct {
		name string
		g    *Generator
		args args
	}{
		{
			"review_Error",
			NewGenerator(doc),
			args{name: "Error", schema: doc.Get("components", "schemas", "Error")},
		},
		{
			"review_NewReview",
			NewGenerator(doc),
			args{name: "NewReview", schema: doc.Get("components", "schemas", "NewReview")},
		},
		{
			"review_Review",
			NewGenerator(doc),
			args{name: "Review", schema: doc.Get("components", "schemas", "Review")},
		},
		{
			"review_Incident",
			NewGenerator(doc),
			args{name: "Incident", schema: doc.Get("components", "schemas", "Incident")},
		},
		{
			"review_Session",
			NewGenerator(doc),
			args{name: "Session", schema: doc.Get("components", "schemas", "Session")},
		},
		{
			"review_Breach",
			NewGenerator(doc),
			args{name: "Breach", schema: doc.Get("components", "schemas", "Breach")},
		},
		{
			"review_Breach_Tagged",
			NewGenerator(doc),
			args{name: "Breach", schema: doc.Get("components", "schemas", "Breach"), tag: []tagFunc{TagJsonSnakeCase}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.g.Type(tt.args.name, tt.args.schema, tt.args.tag...)
			got := tt.g.String()
			// snap
			// os.WriteFile(fmt.Sprintf("../../testdata/%s.txt", tt.name), []byte(got), os.ModePerm)
			want := ioutil.MustReadFile(fmt.Sprintf("../../testdata/%s.txt", tt.name))
			assert.Equal(t, want, got)
		})
	}
}

func TestGenerator_Request(t *testing.T) {
	doc := jsonschema.MustParseFile("../../testdata/petstore.json", false)
	doc2 := jsonschema.MustParseFile("../../testdata/review.yaml", false)
	type args struct {
		operation *fastjson.Value
	}
	tests := []struct {
		name string
		g    *Generator
		args args
	}{
		{
			"uploadFile",
			NewGenerator(doc),
			args{operation: doc.Get("paths", "/pet/{petId}/uploadImage", "post")},
		},
		{
			"StartReview",
			NewGenerator(doc2),
			args{operation: doc2.Get("paths", "/review/start", "post")},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.g.Request(tt.args.operation)
			got := tt.g.String()
			// snap
			// os.WriteFile(fmt.Sprintf("../../testdata/%s-request.txt", tt.name), []byte(got), os.ModePerm)
			want := ioutil.MustReadFile(fmt.Sprintf("../../testdata/%s-request.txt", tt.name))
			assert.Equal(t, want, got)
		})
	}
}

func TestGenerator_Responses(t *testing.T) {
	doc := jsonschema.MustParseFile("../../testdata/petstore.json", false)
	doc2 := jsonschema.MustParseFile("../../testdata/review.yaml", false)
	type args struct {
		operation *fastjson.Value
	}
	tests := []struct {
		name string
		g    *Generator
		args args
	}{
		{
			"uploadFile",
			NewGenerator(doc),
			args{operation: doc.Get("paths", "/pet/{petId}/uploadImage", "post")},
		},
		{
			"AuthenticateReviewer",
			NewGenerator(doc2),
			args{operation: doc2.Get("paths", "/reviewer/auth", "post")},
		},
		{
			"StartReview",
			NewGenerator(doc2),
			args{operation: doc2.Get("paths", "/review/start", "post")},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.g.Responses(tt.args.operation)
			got := tt.g.String()
			// snap
			// os.WriteFile(fmt.Sprintf("../../testdata/%s-response.txt", tt.name), []byte(got), os.ModePerm)
			want := ioutil.MustReadFile(fmt.Sprintf("../../testdata/%s-response.txt", tt.name))
			assert.Equal(t, want, got)
		})
	}
}

func TestGenerator_ServiceProvider(t *testing.T) {
	doc := jsonschema.MustParseFile("../../testdata/petstore.json", false)
	doc2 := jsonschema.MustParseFile("../../testdata/review.yaml", false)
	type args struct {
		paths *fastjson.Value
	}
	tests := []struct {
		name string
		g    *Generator
		args args
	}{
		{
			"petstore",
			NewGenerator(doc),
			args{paths: doc.Get("paths")},
		},
		{
			"review",
			NewGenerator(doc2),
			args{paths: doc2.Get("paths")},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.g.ServiceProvider(tt.args.paths)
			got := tt.g.String()
			// snap
			// os.WriteFile(fmt.Sprintf("../../testdata/%s-service.txt", tt.name), []byte(got), os.ModePerm)
			want := ioutil.MustReadFile(fmt.Sprintf("../../testdata/%s-service.txt", tt.name))
			assert.Equal(t, want, got)
		})
	}
}

func TestGenerator_OperationsHandler(t *testing.T) {
	doc := jsonschema.MustParseFile("../../testdata/petstore.json", false)
	doc2 := jsonschema.MustParseFile("../../testdata/review.yaml", false)
	type args struct {
		paths *fastjson.Value
	}
	tests := []struct {
		name string
		g    *Generator
		args args
	}{
		{
			"petstore",
			NewGenerator(doc),
			args{paths: doc.Get("paths")},
		},
		{
			"review",
			NewGenerator(doc2),
			args{paths: doc2.Get("paths")},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.g.OperationHandlers(tt.args.paths)
			got := tt.g.String()
			// snap
			// os.WriteFile(fmt.Sprintf("../../testdata/%s-ops.txt", tt.name), []byte(got), os.ModePerm)
			want := ioutil.MustReadFile(fmt.Sprintf("../../testdata/%s-ops.txt", tt.name))
			assert.Equal(t, want, got)
		})
	}
}

func TestGenerator_ServiceHandler(t *testing.T) {
	doc := jsonschema.MustParseFile("../../testdata/petstore.json", false)
	doc2 := jsonschema.MustParseFile("../../testdata/review.yaml", false)
	type args struct {
		paths *fastjson.Value
	}
	tests := []struct {
		name string
		g    *Generator
		args args
	}{
		{
			"petstore",
			NewGenerator(doc),
			args{paths: doc.Get("paths")},
		},
		{
			"review",
			NewGenerator(doc2),
			args{paths: doc2.Get("paths")},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.g.ServiceHandler(tt.args.paths)
			got := tt.g.String()
			// snap
			// FIX ME: hash keys are unsorted, fails the test randomly
			os.WriteFile(fmt.Sprintf("../../testdata/%s-server.txt", tt.name), []byte(got), os.ModePerm)
			want := ioutil.MustReadFile(fmt.Sprintf("../../testdata/%s-server.txt", tt.name))
			// fmt.Println(got)
			assert.Equal(t, want, got)
		})
	}
}

func TestGenerator_All(t *testing.T) {
	tests := []struct {
		name string
		g    *Generator
	}{
		{
			"petstore",
			NewGenerator(jsonschema.MustParseFile("../../testdata/petstore.json", false)),
		},
		{
			"bookstore",
			NewGenerator(jsonschema.MustParseFile("../../testdata/bookstore.json", false)),
		},
		{
			"review",
			NewGenerator(jsonschema.MustParseFile("../../testdata/review.yaml", false)),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.g.All(tt.name + "_v1")
			got := tt.g.String()
			// snap
			// FIX ME: hash keys are unsorted, fails the test randomly
			os.WriteFile(fmt.Sprintf("../../testdata/%s-all.txt", tt.name), []byte(got), os.ModePerm)
			want := ioutil.MustReadFile(fmt.Sprintf("../../testdata/%s-all.txt", tt.name))
			assert.Equal(t, want, got)
		})
	}
}
