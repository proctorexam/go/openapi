package specification_v3

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/google/gnostic/printer"
	"github.com/valyala/fastjson"
	"gitlab.com/proctorexam/go/openapi/internal/jsonschema"
	"gitlab.com/proctorexam/go/openapi/internal/stringutil"
)

type Code struct {
	p *printer.Code
}

func NewCode() *Code {
	return &Code{p: &printer.Code{}}
}

func (c *Code) Indent() *Code {
	c.p.Indent()
	return c
}
func (c *Code) Outdent() *Code {
	c.p.Outdent()
	return c
}
func (c *Code) Print(args ...interface{}) *Code {
	c.p.Print(args...)
	return c
}
func (c *Code) PrintIf(condition bool, args ...interface{}) *Code {
	c.p.PrintIf(condition, args...)
	return c
}
func (c *Code) String() string {
	return c.p.String()
}

type Generator struct {
	document *fastjson.Value
	code     *Code
}

func NewGenerator(document *fastjson.Value) *Generator {
	return &Generator{document: document, code: NewCode()}
}

func (g *Generator) String() string {
	return g.code.String()
}

func (g *Generator) Type(name string, schema *fastjson.Value, tag ...tagFunc) {
	nType := FindNativeType(schema, g.document)
	tName := eName(name)
	if nType == "" {
		log.Println("WARNING: native type not found for", name, schema)
		return
	}
	if nType == "struct" {
		g.Struct(name, schema, tag...)
		return
	}
	g.code.Print("type %s %s", tName, nType)
}

func (g *Generator) Struct(name string, schema *fastjson.Value, tag ...tagFunc) {
	tName := eName(name)
	g.code.
		Print("type %s struct {", tName).
		Indent()
	schema.GetObject("properties").Visit(func(fKey []byte, fSchema *fastjson.Value) {
		g.StructField(string(fKey), fSchema, tag...)
	})
	g.code.
		Outdent().
		Print("}")
}

func (g *Generator) StructField(name string, schema *fastjson.Value, tag ...tagFunc) {
	nType := FindNativeType(schema, g.document)
	tName := eName(name)
	tTag := ""
	tags := make([]string, len(tag))
	for i, t := range tag {
		tags[i] = t(tName)
	}
	if len(tags) > 0 {
		tTag = fmt.Sprintf("`%s`", strings.Join(tags, " "))
	}

	if nType == "" {
		log.Println("WARNING: native type not found for", name, schema)
		return
	}
	if nType == "struct" {
		g.code.
			Print("%s struct {", tName).
			Indent()
		schema.GetObject("properties").Visit(func(fKey []byte, fSchema *fastjson.Value) {
			g.StructField(string(fKey), fSchema, tag...)
		})
		g.code.
			Outdent().
			Print("} %s", tTag)
		return
	}
	g.code.Print("%s %s %s", tName, nType, tTag)
}

// NOTE: only single content type supported, requestBody/content cannot have multiple types
// NOTE: non application/json request body content type receives raw io.Reader as Payload field
// NOTE: parameters are not being decoded and passed to operation as string or []string
func (g *Generator) Request(operation *fastjson.Value) {
	oId := str(operation, "operationId")
	if oId == "" {
		log.Println("WARNING, operationId missing", operation)
		return
	}
	oName := eName(oId)
	var payload *payloadType
	operation.GetObject("requestBody", "content").Visit(func(cType []byte, content *fastjson.Value) {
		if payload != nil {
			return
		}
		nType := FindNativeType(content.Get("schema"), g.document)
		fName := "Payload"
		if nType[0] == '*' {
			fName = eName(nType[1:])
		}
		if nType != "" {
			payload = &payloadType{
				contentType:   string(cType),
				nativeType:    nType,
				parameterName: fName,
			}
		}
	})

	var pathParameters []*fastjson.Value
	g.document.GetObject("paths").Visit(func(pathPattern []byte, pathItem *fastjson.Value) {
		pathItem.GetObject().Visit(func(method []byte, op *fastjson.Value) {
			if str(op, "operationId") == oId {
				pathParameters = pathItem.GetArray("parameters")
			}
		})
	})
	parameters := append(pathParameters, operation.GetArray("parameters")...)
	if payload == nil && len(parameters) == 0 {
		return
	}

	// generate type
	g.code.
		Print("type %sRequest struct {", oName).
		Indent()
	for _, p := range parameters {
		nType := FindNativeType(p.Get("schema"), g.document)
		if nType == "" {
			continue
		}
		pName := str(p, "name")
		fName := eName(pName)
		g.code.Print("%s %s", fName, nType)
	}
	if payload != nil {
		g.code.PrintIf(payload.contentType == "application/json", "%s %s", payload.parameterName, payload.nativeType)
		g.code.PrintIf(payload.contentType != "application/json", "%s io.Reader", payload.parameterName)
	}
	g.code.
		Outdent().
		Print("}")

	// generate decoder
	g.code.
		Print("func (out *%sRequest) Decode(in *http.Request) error {", oName).
		Indent()
	urlPartsWritted := false
	for _, p := range parameters {
		pSchema := p.Get("schema")
		isArray := str(pSchema, "type") == "array"
		nType := FindNativeType(pSchema, g.document)
		if nType == "" {
			log.Println("WARNING: native type not found for parameter", p)
			continue
		}
		pName := str(p, "name")
		fName := eName(pName)
		switch str(p, "in") {
		case "path":
			var pPattern string
			g.document.GetObject("paths").Visit(func(pathPattern []byte, pathItem *fastjson.Value) {
				pathItem.GetObject().Visit(func(method []byte, op *fastjson.Value) {
					if str(op, "operationId") == oId {
						pPattern = string(pathPattern)
					}
				})
			})
			if pPattern == "" {
				log.Println("WARNING: operation path not found", oId)
				continue
			}
			pNamePattern := fmt.Sprintf("{%s}", pName)
			for i, pp := range strings.Split(pPattern, "/") {
				if pp == pNamePattern {
					if !urlPartsWritted {
						g.code.Print(`parts := strings.Split(in.URL.Path, "/")`)
						urlPartsWritted = true
					}
					g.code.
						Print(`if len(parts) > %v {`, i).
						Indent().
						Print(`out.%s = parts[%v]`, eName(pName), i).
						Outdent().
						Print(`}`)
					break
				}
			}
		case "query":
			g.code.
				PrintIf(isArray, `out.%s = in.URL.Query()["%s"]`, eName(fName), pName).
				PrintIf(!isArray, `out.%s = in.URL.Query().Get("%s")`, eName(fName), pName)
		case "header":
			g.code.
				PrintIf(isArray, `out.%s = in.Header()["%s"]`, eName(fName), pName).
				PrintIf(!isArray, `out.%s = in.Header().Get("%s")`, eName(fName), pName)
		case "cookie":
			g.code.
				Print(`for _, c := range in.Cookies() {`).
				Indent().
				Print(`if c.Name == "%s" {`, pName).
				Indent().
				Print(`out.%s = c.Value`, eName(fName)).
				Print(`break`).
				Outdent().
				Print(`}`).
				Outdent().
				Print(`}`)
		}
	}
	if payload != nil {
		g.code.
			PrintIf(payload.contentType == "application/json", "if err := json.NewDecoder(in.Body).Decode(&out.%s); err != nil { return err }", payload.parameterName).
			PrintIf(payload.contentType != "application/json", "out.%s = in.Body", payload.parameterName)
	}

	g.code.
		Print(`return nil`).
		Outdent().
		Print("}")
}

func (g *Generator) Responses(operation *fastjson.Value) {
	oId := str(operation, "operationId")
	if oId == "" {
		log.Println("WARNING, operationId missing", operation)
		return
	}
	oName := eName(oId)
	g.code.
		Print("type %sResponses struct {", oName).
		Indent().
		Print("r http.ResponseWriter").
		Outdent().
		Print("}")
	operation.GetObject("responses").Visit(func(c []byte, v *fastjson.Value) {
		status := string(c)
		// NOTE: asuming default response is an error!
		var statusCode int = 0
		if status != "default" {
			statusCode, _ = strconv.Atoi(status)
			if statusCode == 0 {
				log.Println("WARNING: non status code response key:", status)
				return
			}
		}
		args := make([]string, 0)
		headers := make(map[string]string)
		if v.Exists("$ref") {
			v = jsonschema.Resolve(g.document, v.Get("$ref"))
		}
		v.GetObject("headers").Visit(func(n []byte, header *fastjson.Value) {
			nType := FindNativeType(header.Get("schema"), g.document)
			if nType == "" {
				return
			}
			hName := string(n)
			fName := eName(hName)
			fName = fmt.Sprintf("%s%s", strings.ToLower(fName[0:1]), fName[1:])
			args = append(args, fmt.Sprintf("%s %s", fName, nType))
			headers[hName] = fName
		})
		var payload *payloadType
		v.GetObject("content").Visit(func(cType []byte, content *fastjson.Value) {
			if payload != nil {
				return
			}
			nType := FindNativeType(content.Get("schema"), g.document)
			if nType != "" {
				fName := "payload"
				if nType[0] == '*' {
					fName = fmt.Sprintf("%s%s", strings.ToLower(nType[1:2]), nType[2:])
				}
				payload = &payloadType{
					contentType:   string(cType),
					nativeType:    nType,
					parameterName: fName,
				}
				args = append(args, fmt.Sprintf("%s %s", fName, payload.nativeType))
			}
		})
		funcName := "Default"
		if statusCode > 0 {
			funcName = eName(http.StatusText(statusCode))
		} else {
			statusCode = 400
		}
		g.code.
			Print("func (w %sResponses) %s(%s) {", oName, funcName, strings.Join(args, ", ")).
			Indent()
		for k, v := range headers {
			g.code.Print(`w.r.Header().Set("%s", %s)`, k, v)
		}
		g.code.Print("w.r.WriteHeader(%v)", statusCode)
		if payload != nil {
			switch payload.contentType {
			case "application/json":
				g.code.
					Print("if err := json.NewEncoder(w.r).Encode(%s); err != nil {", payload.parameterName).
					Indent().
					Print(`log.Println("ERROR: response encoding error: ", err)`).
					Outdent().
					Print("}")
			case "[]byte":
				g.code.
					Print("if _, err := w.r.Write(%s); err != nil {", payload.parameterName).
					Indent().
					Print(`log.Println("ERROR: response encoding error: ", err)`).
					Outdent().
					Print("}")
			default:
				log.Println("WARNING: unsupported response content type", payload.contentType)
			}
		}
		g.code.
			Outdent().
			Print("}")
	})
}

func (g *Generator) ServiceProvider(paths *fastjson.Value) {
	g.code.
		Print("type ServiceProvider interface {").
		Indent()
	paths.GetObject().Visit(func(pathPattern []byte, pathItem *fastjson.Value) {
		pathItem.GetObject().Visit(func(methodName []byte, operation *fastjson.Value) {
			oId := str(operation, "operationId")
			if oId == "" {
				log.Println("WARNING, operationId missing", operation)
				return
			}
			oName := eName(oId)
			hasRequest := operation.Exists("parameters") || operation.Exists("requestBody")
			g.code.
				PrintIf(hasRequest, "%s(ctx context.Context, req *%sRequest, res *%sResponses) error", oName, oName, oName).
				PrintIf(!hasRequest, "%s(ctx context.Context, res *%sResponses) error", oName, oName)

		})
	})
	g.code.
		Outdent().
		Print("}")
}

func (g *Generator) OperationHandlers(paths *fastjson.Value) {
	paths.GetObject().Visit(func(pathPattern []byte, pathItem *fastjson.Value) {
		pathItem.GetObject().Visit(func(methodName []byte, operation *fastjson.Value) {
			oId := str(operation, "operationId")
			if oId == "" {
				log.Println("WARNING, operationId missing", operation)
				return
			}
			oName := eName(oId)
			hasRequest := operation.Exists("parameters") || operation.Exists("requestBody")
			// hasResponse := false
			// operation.GetObject("responses").Visit(func(status []byte, response *fastjson.Value) {
			// 	if response.Exists("headers") || response.Exists("content") {
			// 		hasResponse = true
			// 	}
			// })

			if hasRequest {
				g.code.
					Print(`func %sHandler(service ServiceProvider) http.HandlerFunc {`, oName).
					Indent().
					Print(`return func(w http.ResponseWriter, r *http.Request) {`).
					Indent().
					Print(`req := &%sRequest{}`, oName).
					Print(`if err := req.Decode(r); err != nil {`).
					Indent().
					Print(`w.WriteHeader(http.StatusBadRequest)`).
					Print(`return`).
					Outdent().
					Print(`}`).
					Print(`if err := service.%s(r.Context(), req, &%sResponses{w}); err != nil {`, oName, oName).
					Indent().
					Print(`w.WriteHeader(http.StatusInternalServerError)`).
					Print(`return`).
					Outdent().
					Print(`}`).
					Outdent().
					Print(`}`).
					Outdent().
					Print(`}`)
			} else {
				g.code.
					Print(`func %sHandler(service ServiceProvider) http.HandlerFunc {`, oName).
					Indent().
					Print(`return func(w http.ResponseWriter, r *http.Request) {`).
					Indent().
					Print(`if err := service.%s(r.Context(), &%sResponses{w}); err != nil {`, oName, oName).
					Indent().
					Print(`w.WriteHeader(http.StatusInternalServerError)`).
					Print(`return`).
					Outdent().
					Print(`}`).
					Outdent().
					Print(`}`).
					Outdent().
					Print(`}`)
			}
		})
	})
}

func (g *Generator) ServiceHandler(paths *fastjson.Value) {
	// func ServiceHandler(service ServiceProvider, auth AuthProvider) http.HandlerFunc {
	// 	paths := map[int]map[string]map[string]http.HandlerFunc{
	// 		2: {
	// 			"POST": {
	// 				"/review/start": BearerAuthHandler(auth, StartReviewHandler(service)),
	// 			},
	// 		},
	// 	}

	// 	return func(w http.ResponseWriter, r *http.Request) {
	// 		op := matchOp(r.URL.Path, r.Method, paths)
	// 		if op == nil {
	// 			w.WriteHeader(http.StatusNotFound)
	// 			return
	// 		}
	// 		op(w,r)
	// 		return
	// 	}
	// }
	pathsMap := make(map[int]map[string]map[string]string)

	paths.GetObject().Visit(func(pathBytes []byte, pathItem *fastjson.Value) {
		pathPattern := string(pathBytes)
		pathParts := strings.Split(pathPattern, "/")
		n := len(pathParts)
		methodMap, ok := pathsMap[n]
		if !ok {
			pathsMap[n] = make(map[string]map[string]string)
			methodMap = pathsMap[n]
		}

		pathItem.GetObject().Visit(func(methodBytes []byte, operation *fastjson.Value) {
			methodName := strings.ToUpper(string(methodBytes))
			handlerMap, ok := methodMap[methodName]
			if !ok {
				methodMap[methodName] = make(map[string]string)
				handlerMap = methodMap[methodName]
			}

			oId := str(operation, "operationId")
			if oId == "" {
				log.Println("WARNING, operationId missing", operation)
				return
			}
			oName := eName(oId)
			security := operation.GetArray("security")

			for _, sec := range security {
				sec.GetObject().Visit(func(sName []byte, sOpts *fastjson.Value) {
					scheme := str(g.document, "components", "securitySchemes", string(sName), "scheme")
					handlerMap[pathPattern] = fmt.Sprintf(`%sAuthHandler(auth, %sHandler(service)`, eName(scheme), oName)
				})
			}
		})
	})

	g.code.
		Print(`func ServiceHandler(service ServiceProvider, auth AuthProvider) http.HandlerFunc {`).
		Indent().
		Print(`paths := map[int]map[string]map[string]http.HandlerFunc{`).
		Indent()
	for i, methodMap := range pathsMap {
		g.code.
			Print(`%v: {`, i).
			Indent()
		for m, handlerMap := range methodMap {
			if len(handlerMap) == 0 {
				continue
			}
			g.code.
				Print(`"%s": {`, m).
				Indent()
			for pattern, handler := range handlerMap {
				g.code.Print(`"%s": %s),`, pattern, handler)
			}
			g.code.
				Outdent().
				Print(`},`)
		}
		g.code.
			Outdent().
			Print(`},`)
	}
	g.code.
		Outdent().
		Print(`}`).
		Print(`return OperationHandler(paths)`).
		Outdent().
		Print(`}`)
}

func (g *Generator) All(pkgName string) {
	g.code.Print(`
// GENERATED FILE: DO NOT EDIT!

package ` + pkgName + `

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

type ctxKey string

type AuthProvider interface {
	Basic(username, password string) (any, error)
	Bearer(token string) (any, error)
}

func BasicAuthHandler(provider AuthProvider, opHandler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		auth, err := provider.Basic(username, password)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		opHandler(w, r.Clone(context.WithValue(r.Context(), ctxKey("auth"), auth)))
	}
}
func BearerAuthHandler(provider AuthProvider, opHandler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")
		if token == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		auth, err := provider.Bearer(token)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		opHandler(w, r.Clone(context.WithValue(r.Context(), ctxKey("auth"), auth)))
	}
}
func OperationHandler(paths map[int]map[string]map[string]http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		op := matchOp(r.URL.Path, r.Method, paths)
		if op == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		op(w, r)
	}
}
func matchOp(urlPath, httpMethod string, paths map[int]map[string]map[string]http.HandlerFunc) http.HandlerFunc {
	pParts := strings.Split(urlPath, "/")
	pLen := len(pParts)
	for pPatternStr, opHandler := range paths[pLen][httpMethod] {
		pPattern := strings.Split(pPatternStr, "/")
		if matchPath(pParts, pPattern) {
			return opHandler
		}
	}
	return nil
}
func matchPath(pParts, pPattern []string) bool {
	for i, k := range pPattern {
		if len(k) == 0 {
			continue
		}
		if k[0] == '{' {
			continue
		}
		if pParts[i] != k {
			return false
		}
	}
	return true
}
`)

	g.code.Print("")

	g.document.GetObject("components", "schemas").Visit(func(k []byte, v *fastjson.Value) {
		g.Type(eName(string(k)), v, TagJsonSnakeCase)
	})
	paths := g.document.Get("paths")
	paths.GetObject().Visit(func(pathPattern []byte, pathItem *fastjson.Value) {
		pathItem.GetObject().Visit(func(methodName []byte, operation *fastjson.Value) {
			g.Request(operation)
			g.Responses(operation)
		})
	})
	g.OperationHandlers(paths)
	g.ServiceHandler(paths)
	g.ServiceProvider(paths)
}

func (g *Generator) Implementation(pkgName string) {
	g.code.Print(`
	package %s

	import(
		"net/http"
		"context"
	)

	type service struct{}
	`, pkgName)

	g.code.Print("")

	g.document.GetObject("paths").Visit(func(pathPattern []byte, pathItem *fastjson.Value) {
		pathItem.GetObject().Visit(func(methodName []byte, operation *fastjson.Value) {
			oId := str(operation, "operationId")
			if oId == "" {
				log.Println("WARNING, operationId missing", operation)
				return
			}
			oName := eName(oId)
			hasRequest := operation.Exists("parameters") || operation.Exists("requestBody")
			g.code.
				PrintIf(hasRequest, "func (s *service)  %s(ctx context.Context, req *%sRequest, res *%sResponses) error { return nil }", oName, oName, oName).
				PrintIf(!hasRequest, "func (s *service) %s(ctx context.Context, res *%sResponses) error { return nil }", oName, oName)

		})
	})

	g.code.Print("")

	g.code.Print(`
	type auth struct{}

	func (a *auth) Basic(username, password string) (any, error) {
		return nil, nil
	}
	func (a *auth) Bearer(token string) (any, error) {
		return nil, nil
	}

	func main() {
		http.ListenAndServe(":8080", ServiceHandler(&service{}, &auth{}))
	}
	`)
}

func str(v *fastjson.Value, k ...string) string {
	return string(v.GetStringBytes(k...))
}

type tagFunc func(...string) string

func TagJsonSnakeCase(keys ...string) string {
	switch len(keys) {
	case 0:
		return ""
	case 1:
		return fmt.Sprintf(`json:"%s,omitempty"`, stringutil.ToSnakeCase(keys[0]))
	default:
		return fmt.Sprintf(`json:"%s,%s,omitempty"`, stringutil.ToSnakeCase(keys[0]), strings.Join(keys[1:], ","))
	}
}

func cleanName(originalName string) string {
	name := originalName
	name = strings.Replace(name, "application/json", "", -1)
	name = strings.Replace(name, ".", "_", -1)
	name = strings.Replace(name, "-", "_", -1)
	name = strings.Replace(name, " ", "", -1)
	name = strings.Replace(name, "(", "", -1)
	name = strings.Replace(name, ")", "", -1)
	name = strings.Replace(name, "{", "", -1)
	name = strings.Replace(name, "}", "", -1)
	name = strings.Replace(name, "/", "_", -1)
	name = strings.Replace(name, "$", "", -1)
	return name
}

func eName(originalName string) string {
	name := cleanName(originalName)
	name = stringutil.ToCamelCase(name)
	return name
}

type payloadType struct {
	contentType   string
	nativeType    string
	parameterName string
}
