package ioutil

import (
	"log"
	"os"
)

func MustReadFile(filename string) string {
	b, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	return string(b)
}
