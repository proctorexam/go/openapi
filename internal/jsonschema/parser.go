package jsonschema

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"

	"github.com/google/gnostic/jsonschema"
	"github.com/valyala/fastjson"
	"gopkg.in/yaml.v3"
)

func MustParseFile(name string, deref bool) *fastjson.Value {
	doc, err := ParseFile(name, deref)
	if err != nil {
		log.Fatal(err)
	}
	return doc
}

func ParseFile(name string, deref bool) (*fastjson.Value, error) {
	switch extname := path.Ext(name); extname {
	case ".yaml", ".yml":
		input, err := yamlToJson(name)
		if err != nil {
			return nil, err
		}
		return Parse(input, deref)
	case ".json":
		b, err := os.ReadFile(name)
		if err != nil {
			return nil, err
		}
		return Parse(string(b), deref)
	default:
		return nil, fmt.Errorf("invalid file extension %s. expected yml, yaml or json", extname)
	}
}

func MustParse(s string, deref bool) *fastjson.Value {
	doc, err := Parse(s, deref)
	if err != nil {
		log.Fatal(err)
	}
	return doc
}

func Parse(s string, deref bool) (*fastjson.Value, error) {
	root, err := fastjson.Parse(s)
	if err != nil {
		return nil, err
	}
	if deref {
		dereference(root, root.GetObject())
	}
	return root, nil
}

func ParseRef(ref *fastjson.Value) (keys []string, end int) {
	refstr := string(ref.GetStringBytes())
	if refstr == "" || refstr[0:2] != "#/" {
		return
	}
	keys = strings.Split(refstr[2:], "/")
	length := len(keys)
	if length < 1 {
		return
	}
	end = length - 1
	return
}

func Resolve(root *fastjson.Value, ref *fastjson.Value) *fastjson.Value {
	keys, end := ParseRef(ref)
	return root.GetObject(keys[0:end]...).Get(keys[end])
}

func dereference(root *fastjson.Value, o *fastjson.Object) {
	o.Visit(func(kb []byte, v *fastjson.Value) {
		if ref := v.Get("$ref"); ref != nil {
			if refval := resolve(root, ref); refval != nil {
				o.Set(string(kb), refval)
			}
		} else {
			vo, err := v.Object()
			if err == nil {
				dereference(root, vo)
				return
			}
			va, err := v.Array()
			if err == nil {
				for i, vv := range va {
					vvo, err := vv.Object()
					if err == nil {
						if ref := vvo.Get("$ref"); ref != nil {
							if refval := resolve(root, ref); refval != nil {
								va[i] = refval
							}
						} else {
							dereference(root, vvo)
						}
					}
				}
			}
		}
	})
}

func resolve(root, ref *fastjson.Value) *fastjson.Value {
	val := Resolve(root, ref)
	if val != nil {
		val.Set("x-ref", ref)
	}
	return val
}

func yamlToJson(filename string) (string, error) {
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	var node yaml.Node
	err = yaml.Unmarshal(file, &node)
	if err != nil {
		return "", err
	}
	return jsonschema.Render(&node), nil
}
