package jsonschema

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fastjson"
	"gitlab.com/proctorexam/go/openapi/internal/ioutil"
)

func TestParseFileAndDereference(t *testing.T) {
	type args struct {
		filename string
		deref    bool
	}
	tests := []struct {
		name      string
		args      args
		expect    *fastjson.Value
		assertErr assert.ErrorAssertionFunc
	}{
		{
			"parameters yaml",
			args{filename: "../../testdata/parameters.yaml", deref: false},
			fastjson.MustParse(ioutil.MustReadFile("../../testdata/parameters-parsed.json")),
			assert.NoError,
		},
		{
			"bookstore json dereferenced",
			args{filename: "../../testdata/bookstore.json", deref: true},
			fastjson.MustParse(ioutil.MustReadFile("../../testdata/bookstore-parsed-deref.json")),
			assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual, err := ParseFile(tt.args.filename, tt.args.deref)
			tt.assertErr(t, err)
			assert.Equal(t, tt.expect.GetStringBytes(), actual.GetStringBytes())
		})
	}
}

func TestResolve(t *testing.T) {
	type args struct {
		root *fastjson.Value
		ref  *fastjson.Value
	}
	doc := fastjson.MustParse(ioutil.MustReadFile("../../testdata/parameters-parsed.json"))
	tests := []struct {
		name   string
		args   args
		expect *fastjson.Value
	}{
		{
			"parameters yaml",
			args{
				root: doc,
				ref:  doc.GetArray("paths", "/example/query/ref", "get", "parameters")[0].Get("$ref"),
			},
			doc.Get("components", "parameters", "ID"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual := Resolve(tt.args.root, tt.args.ref)
			assert.Equal(t, tt.expect.GetStringBytes(), actual.GetStringBytes())
		})
	}
}

func Test_resolve(t *testing.T) {
	type args struct {
		root *fastjson.Value
		ref  *fastjson.Value
	}
	tests := []struct {
		name string
		args args
		want *fastjson.Value
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := resolve(tt.args.root, tt.args.ref); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("resolve() = %v, want %v", got, tt.want)
			}
		})
	}
}
