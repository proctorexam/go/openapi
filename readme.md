# openapi

go code generator for open api specifications.

## milestone 0

- [x] go type for [schema object](https://spec.openapis.org/oas/latest.html#schema-object)

- [x] http request decoder for [parameter object](https://spec.openapis.org/oas/latest.html#parameter-object)

- [x] http request decoder for [request body](https://spec.openapis.org/oas/latest.html#request-body-object)

- [x] http response encoder for [response object](https://spec.openapis.org/oas/latest.html#response-object)

- [x] func for [operation object](https://spec.openapis.org/oas/latest.html#operation-object)

## milestone 0.1

- [x] http request authenticator [securty scheme object](https://spec.openapis.org/oas/latest.html#security-scheme-object)
- [x] go server
- [ ] js client

## milestone 0.2

- [ ] test generation

## milestone 0.x

- [ ] open source contribution
- [ ] rest of the spec...
