module gitlab.com/proctorexam/go/openapi

go 1.19

require (
	github.com/google/gnostic v0.6.9
	github.com/stretchr/testify v1.5.1
	github.com/valyala/fastjson v1.6.3
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.3 // indirect
)
