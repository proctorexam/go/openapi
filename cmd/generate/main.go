package main

import (
	"flag"
	"os"
	"path"

	"gitlab.com/proctorexam/go/openapi/internal/jsonschema"
	specification_v3 "gitlab.com/proctorexam/go/openapi/specification/v3"
)

var (
	pkgName = flag.String("pkg", "main", "package name for the generated code, default is main")
)

func main() {
	flag.Parse()
	filename := flag.Args()[0]
	outdir := flag.Args()[1]
	os.MkdirAll(outdir, os.ModePerm)
	g := specification_v3.NewGenerator(jsonschema.MustParseFile(filename, false))
	g.All(*pkgName)
	os.WriteFile(path.Join(outdir, "service.go"), []byte(g.String()), os.ModePerm)
	g = specification_v3.NewGenerator(jsonschema.MustParseFile(filename, false))
	g.Implementation(*pkgName)
	os.WriteFile(path.Join(outdir, "main.go"), []byte(g.String()), os.ModePerm)
}
